<form action="" method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="nome">Nome</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="nome" name="nome" value="">
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="email">E-mail</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="email" name="email" value="">
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="senha">Password</label>
        <div class="col-sm-5">
            <input class="form-control" type="text" id="senha" name="senha" value="">
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="perfil">Perfil</label>
        <div class="col-sm-5">
            <input class="form-control" type="text" id="perfil" name="perfil" value="">
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="foto">Foto</label>
        <div class="col-sm-5">
            <img class="img-fluid" src="https://via.placeholder.com/200x200">
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Permissão</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="0">Colaborador</option>
                <option value="1">Editor</option>
                <option value="2">Adiministrador</option>
            </select>
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

</form>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Status</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="0">Ativo</option>
                <option value="1">Inativo</option>
            </select>
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

    <div class="for-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Salvar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div><!--/.offset-sm-2 col-sm-10 -->
    </div><!--/.form-group row -->
</form>