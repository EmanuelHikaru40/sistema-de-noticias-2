@extends('layouts.admin')

@section('titulo','Área administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Cadastrar Usuários</h2>
        </div><!--/.col-12 -->
    </div><!--/.row -->

    <div class="row mt-3">
        <div class="col-12">
            @include('admin.usuarios.form')
        </div><!--/.col-12 -->
    </div><!--/.row mt-3 -->

</div><!--/.container -->

@endsection