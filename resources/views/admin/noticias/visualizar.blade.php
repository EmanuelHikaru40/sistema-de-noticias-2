@extends('layouts.admin')

@section('titulo','Área administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Visualizar Notícia</h2>
        </div><!--/.col-12 -->
    </div><!--/.row -->

    <div class="row mt-3">
        <div class="col-12">
            <table class="table table-striped table-condensed">
                <tr>
                    <th width="150">ID</th>
                    <td>1</td>
                </tr>
                <tr>
                    <th width="150">Título</th>
                    <td>Tem jogo de volta</td>
                </tr>
                <tr>
                    <th width="150">Subtitulo</th>
                    <td>Tem jogo de volta</td>
                </tr>
                <tr>
                    <th width="150">Descrição</th>
                    <td>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quasi id explicabo hic quod reprehenderit error soluta ex, similique illum labore inventore reiciendis, veritatis eos? A ratione mollitia praesentium cupiditate adipisci.</td>
                </tr>
                <tr>
                    <th width="150">Status</th>
                    <td>Não Publicado</td>
                </tr>
            </table>
                <a href="#" class="btn btn-danger">Editar Noticia</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
              

        </div><!--/.col-12 -->
    </div><!--/.row mt-3 -->

    

</div><!--/.container -->

@endsection