<form action="" method="POST">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="titulo">Titulo</label>
        <div class="col-sm-10">
        <input class="form-control" type="text" id="titulo" name="titulo" value="">
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">Subtitulo</label>
        <div class="col-sm-10">
        <input class="form-control" type="text" id="subtitulo" name="subtitulo" value="">
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Descrição</label>
        <div class="col-sm-10">
        <textarea class="form-control" id="descricao" name="descricao"></textarea>
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="status">Status</label>
        <div class="col-sm-5">
        <select class="form-control" id="status" name="descricao">
            <option value="0">Não Publicado</option>
            <option value="1">Aguardando Revisão</option>
            <option value="2">Publicado</option>
        </select>
        </div><!--/.col-sm-5 -->
    </div><!--/.form-group row -->

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-danger">Enviar</button>
        <a href="#" class="btn btn-secondary">Cancelar</a>
        </div><!--/.col-sm-10 -->
    </div><!--/.form-group row 2 -->

</form>